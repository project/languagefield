<?php

namespace Drupal\languagefield\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Tags;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\OptGroup;
use Drupal\languagefield\Plugin\Field\FieldType\LanguageItem;

/**
 * Plugin implementation of the 'languagefield_autocomplete_tags' widget.
 *
 * @FieldWidget(
 *   id = "languagefield_autocomplete_tags",
 *   label = @Translation("Language autocomplete (Tags style)"),
 *   field_types = {
 *     "language_field",
 *   },
 *   multiple_values = TRUE,
 * )
 */
class LanguageAutocompleteTagsWidget extends LanguageAutocompleteWidget {

  /**
   * Form element validate handler for language autocomplete tags element.
   *
   * @param mixed $element
   *   Element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public static function validateElement($element, FormStateInterface $form_state): void {
    if (!$value = $element['#value']) {
      return;
    }

    // Create array of ISO-2 codes from the submitted string of languages.
    $values = [];
    $languages = $element['#languagefield_options'];
    $input_values = Tags::explode($value);
    foreach ($input_values as $value) {
      $langcode = array_search($value, $languages);
      if (!empty($langcode)) {
        $values[] = ['value' => $langcode];
      }
    }
    // Make sure all the submitted languages have valid ISO-2 codes.
    if (count($values) === count($input_values)) {
      $form_state->setValueForElement($element, $values);
    }
    else {
      $form_state->setError($element, t('An unexpected language is entered.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $item = $items[$delta];
    if (!$item instanceof LanguageItem) {
      $item = new LanguageItem($items->getItemDefinition());
    }

    // Use all possible languages, in case field contains invalid language.
    $possible_languages = $item->getPossibleOptions();

    // Re-format the list of values to human readable tags.
    $values = [];
    foreach ($items as $item) {
      if ($item->value) {
        $values[] = $possible_languages[$item->value];
      }
    }

    $element['value']['#tags'] = TRUE;
    $element['value']['#default_value'] = count($values) ? Tags::implode($values) : '';
    $element['value']['#description'] = $this->t('When choosing a language
      that contains a comma (E.g: English, British), you should wrap
      the selected language in quotes (E.g: "English, British")');
    $element['value']['#attributes']['class'][] = 'language-autocomplete-tags';

    $element['#attached']['library'][] = 'languagefield/languagefield';

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state): array {
    $value = $values['value'];
    if (is_array($value)) {
      return $value;
    }

    if (is_string($value)) {
      // Create array of ISO-2 codes from the submitted string of languages.
      $values = [];
      $field_definition = $this->fieldDefinition->getFieldStorageDefinition();
      $languages = languagefield_allowed_values($field_definition);
      $languages = OptGroup::flattenOptions($languages);
      $input_values = Tags::explode($value);
      foreach ($input_values as $value) {
        $langcode = array_search($value, $languages);
        if (!empty($langcode)) {
          $values[] = ['value' => $langcode];
        }
      }
      return $values;
    }

    return $value;
  }

}
