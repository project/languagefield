(function ($, Drupal, once) {
  Drupal.behaviors.languageFieldAutocompleteTags = {
    attach(context, settings) {
      once('languageFieldAutocompleteTags', '.language-autocomplete-tags', context).forEach(
        function (element) {

          $(element).on('autocompleteselect', function (event, ui) {
            let selectedValue = ui.item.value;

            // Check if selectedValue contains a comma. If it does and,
            // it is not wrapper in quotes, then wrap it.
            if (selectedValue.includes(',') && !selectedValue.includes('"')) {
              selectedValue = '"' + selectedValue + '"';
            }

            const currentValue = $(element).val();

            // Split the current value into an array.
            let values = currentValue.split(',').
            map(val => val.trim());

            // Replace the last value, since the value of the string was
            // entered by the user, and not autocompleted.
            if (values.length > 0) {
              values[values.length - 1] = selectedValue;
            } else {
              values.push(selectedValue);
            }

            // Use setTimeout to ensure the field is updated correctly.
            setTimeout(() => {
              // Trigger change event to update the field.
              $(element).val(values.join(', ')).trigger('change');
            }, 0);
          });
        }
      )
    }
  };
})(jQuery, Drupal, once);
